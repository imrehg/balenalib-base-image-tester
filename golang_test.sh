#!/usr/bin/env bash

set -o xtrace -o errexit -o pipefail

# shellcheck disable=SC2034
OS=$1
SOFTWARE_VERSION=$2

function check_vesion {
    local queried_go_version

    [ -n "$GO_VERSION" ] || ( echo "No GO_VERSION env var!" ; exit 1 )
    queried_go_version=$(go version)
    [[ "${queried_go_version}" =~ ^go[[:space:]]version[[:space:]]go"${GO_VERSION}"[[:space:]]linux.* ]] || ( echo "Go version doesn't match." ; exit 1 )

    [[ "${queried_go_version}" =~ ^go[[:space:]]version[[:space:]]go"${SOFTWARE_VERSION}".*linux.* ]] || ( echo "Go version and tag doesn't match." ; exit 1)

}

function go_get {
    local result

    go get github.com/golang/example/hello

    result=$(hello)
    [ "$result" == "Hello, Go examples!" ] || ( echo "Go get & run failed." ; exit 1)
}

function main {
    check_vesion
    go_get
}

main