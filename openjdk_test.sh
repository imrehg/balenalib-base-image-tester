#!/usr/bin/env bash

set -o xtrace -o errexit -o pipefail

# shellcheck disable=SC2034
OS=$1
SOFTWARE_VERSION=$2
SOFTWARE_VARIANT=$3

function check_vesion {
    local queried_openjdk_version

    queried_openjdk_version=$(java -version 2>&1 | grep openjdk | awk '{ print $3 }')
    if [ "${SOFTWARE_VERSION}" -lt 10 ]; then
        [[ "${queried_openjdk_version}" =~ ^\"1\."$SOFTWARE_VERSION".* ]] || ( echo "OpenJDK version doesn't match." ; exit 1 )
    else
        [[ "${queried_openjdk_version}" =~ ^\""$SOFTWARE_VERSION".* ]]|| ( echo "OpenJDK version doesn't match." ; exit 1 )
    fi
}

function java_run {
    local result

    if [ "${SOFTWARE_VARIANT}" = "jdk" ]; then
        echo "Compiling Java source"
        cp asset/HelloWorld.java ./
        javac HelloWorld.java
    else
        echo "Use precompiled Java class"
        cp asset/HelloWorld.class ./
    fi
    result=$(java HelloWorld)

    [ "$result" == "check" ] || ( echo "OpenJDK run failed." ; exit 1)
}

function main {
    check_vesion
    java_run
}

main