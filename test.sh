#!/usr/bin/env bash

set -o xtrace -o errexit -o pipefail

function env_vars {
    env
}

function os_version {
    # shellcheck disable=SC1091
    source /etc/os-release
    if [ "${ID}" != "${OS_NAME}" ] ; then
        exit 1
    fi
    if [ -n "$OS_VERSION" ]; then
        case "$OS_NAME" in
            alpine)
                echo "$PRETTY_NAME" | grep -q -i "$OS_VERSION" || (echo "Incorrect OS version"; exit 1)
                ;;
            debian|raspbian)
                echo "$PRETTY_NAME" | grep -q -i "$OS_VERSION" || (echo "Incorrect OS version"; exit 1)
                ;;
            fedora)
                [[ "${VERSION_ID}" == "${OS_VERSION}" ]] || (echo "Incorrect OS version"; exit 1)
                ;;
            ubuntu)
                [[ "${UBUNTU_CODENAME}" == "${OS_VERSION}" ]] || (echo "Incorrect OS version"; exit 1)
                ;;
            *)
                echo "Unsure how to handle OS version for $OS_NAME, skipping test..."
        esac
    fi
}

function os_test {
    case "$OS_NAME" in
        debian|raspbian|ubuntu)
            apt-get update && apt-get install -y -q --no-install-recommends htop
            ;;
        alpine)
            apk update && apk add htop
            ;;
        fedora)
            dnf install -y htop
            ;;
        *)
            echo "No OS test for $OS..."
    esac
}

function main {

    env_vars
    os_version
    os_test

    case "${LANGUAGE}" in
        golang)
            ./golang_test.sh "${OS_NAME}" "${SOFTWARE_VERSION}" "${SOFTWARE_VARIANT}"
            ;;
        node)
            ./node_test.sh "${OS_NAME}" "${SOFTWARE_VERSION}" "${SOFTWARE_VARIANT}"
            ;;
        python)
            ./python_test.sh "${OS_NAME}" "${SOFTWARE_VERSION}" "${SOFTWARE_VARIANT}"
            ;;
        openjdk)
            ./openjdk_test.sh "${OS_NAME}" "${SOFTWARE_VERSION}" "${SOFTWARE_VARIANT}"
            ;;
        dotnet)
            ./dotnet_test.sh "${OS_NAME}" "${SOFTWARE_VERSION}" "${SOFTWARE_VARIANT}"
            ;;
        *)
            echo "No special tests to run"
    esac
}

###
# Script start
###
while getopts ":d:l:o:r:s:x:v:" opt; do
echo "${opt}"
case ${opt} in
    d )
    DEVICE_TYPE=$OPTARG
    ;;
    l )
    LANGUAGE=$OPTARG
    ;;
    o )
    OS_NAME=$OPTARG
    ;;
    r )
    OS_VERSION=$OPTARG
    ;;
    s )
    SOFTWARE_VERSION=$OPTARG
    ;;
    x )
    SOFTWARE_VARIANT=$OPTARG
    ;;
    v )
    VARIANT=$OPTARG
    ;;
    \? )
    echo "Invalid option: $OPTARG" 1>&2
    exit 1
    ;;
    : )
    echo "Invalid option: $OPTARG requires an argument" 1>&2
    exit 1
    ;;
esac
done
shift $((OPTIND -1))

# Custom adjustments
if [ "$OS_NAME" = "debian" ]; then
    case "${DEVICE_TYPE}" in
        rpi|raspberry-pi)
            OS_NAME="raspbian"
            ;;
    esac
fi

echo "Device type: ${DEVICE_TYPE}"
echo "OS name: ${OS_NAME}"
echo "OS version: ${OS_VERSION}"
echo "Language: ${LANGUAGE}"
echo "Software version: ${SOFTWARE_VERSION}"
echo "Software variant: ${SOFTWARE_VARIANT}"
echo "Variant: ${VARIANT}"

main
