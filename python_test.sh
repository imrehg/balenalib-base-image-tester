#!/usr/bin/env bash

set -o xtrace -o errexit -o pipefail

OS=$1
SOFTWARE_VERSION=$2

function check_vesion {
    local queried_python_version
    local queried_pip_version
    local queried_setuptools_version

    [ -n "$PYTHON_VERSION" ] || ( echo "No PYTHON_VERSION env var!" ; exit 1 )
    # Python 2 prints version on STDERR, while Python 3 on STDOUT
    queried_python_version=$(python --version 2>&1)
    if [ "${queried_python_version}" == "Python ${PYTHON_VERSION}" ]; then
        echo "Versions match."
    else
        echo "Versions don't match."
        exit 1
    fi

    [ -n "$PYTHON_PIP_VERSION" ] || ( echo "No PYTHON_PIP_VERSION env var!" ; exit 1 )
    queried_pip_version=$(pip --version)
    if [[ "${queried_pip_version}" =~ ^pip[[:space:]]"${PYTHON_PIP_VERSION}"[[:space:]].* ]]; then
        echo "Versions match."
    else
        echo "Versions don't match."
        exit 1
    fi

    [ -n "$SETUPTOOLS_VERSION" ] || ( echo "No SETUPTOOLS_VERSION env var!" ; exit 1 )
    queried_setuptools_version=$(python -c "import setuptools; print(setuptools.__version__)")
    if [ "${queried_setuptools_version}" == "${SETUPTOOLS_VERSION}" ]; then
        echo "Versions match."
    else
        echo "Versions don't match."
        exit 1
    fi

}

function pip_install {
    pip install flask

    python_args=(-c "import flask")

    if [ "$OS" == "fedora" ]; then
        # Fedora uses system python, so use the relevant exectuable names
        case "$SOFTWARE_VERSION" in
            3*)
                python3 "${python_args[@]}"
                ;;
            *)
                python "${python_args[@]}"
        esac
    else
        python "${python_args[@]}"
    fi
}

function main {
    case "$OS" in
        fedora)
            # Fedora uses system Python packages
            echo "No env var/versions check for Fedora"
            ;;
        *)
            check_vesion
    esac
    pip_install
}

main